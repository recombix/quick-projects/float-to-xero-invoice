import { jest } from 'quickstartconfig';

export default {
	...jest,
	testEnvironment: `node`,
	preset: 'ts-jest',
};
