# Float to Xero Invoice

Convert float time exports from CSV format to a Xero invoice.

## Important Notes

1. You must setup a `.env` file in the repository root, using the `example.env` file for reference.
2. You can run this script as many times as you like each month.
3. You must create an invoice in Xero for the current month before running this script.
4. Each time you run this script it will replace all the items in the invoice.
5. Any line items on the invoice that don't match the prescribed format will be stripped.

## Usage

You must download a CSV "time tracking" report from Float and put it into the `./data` directory and then run this command with the correct name of the file (not the path):

```bash
npm run start:dev <SCOPE> <FILENAMES...>
npm run start:dev 2022-08 PL.csv
```

Multiple CSV files can be combined into a single invoice by passing multiple arguments:

```bash
npm run start:dev 2022-08 PL.csv STR.csv
```
