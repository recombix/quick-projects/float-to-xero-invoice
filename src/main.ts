import * as log from './lib/log';
import * as args from './lib/args';
import * as float from './lib/float';
import * as xero from './lib/xero';
import * as timeEntries from './lib/timeEntries';
import * as config from './config';
import { getTimestampForYearAndMonth } from './lib/utilities';

void (async function main(): Promise<void> {
	try {
		log.info(`Starting script`);

		const [runYear, runMonth, filenames] = args.readCliArgs();
		const runTimestamp = getTimestampForYearAndMonth(runYear, runMonth);
		const entries = await float.readCsvFilesForMonth(filenames, runTimestamp);
		const groupMap = timeEntries.groupTimeEntriesByDate(entries);
		await xero.authenticate();
		const invoice = await xero.getPendingInvoiceForContact(config.env.XERO_CONTACT_ID);
		xero.addLineItemsToInvoice(invoice, groupMap);
		await xero.updateInvoice(invoice);

		log.info(`Success!`);
	} catch (e: unknown) {
		const err = e as NodeJS.ErrnoException;
		log.error(`Script failed =>`, err.stack);
	}
})();
