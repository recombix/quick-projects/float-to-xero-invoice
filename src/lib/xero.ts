import { XeroClient } from 'xero-node';
import * as config from '../config';
import * as log from './log';
import {
	formatInstantAsDateString,
	formatInstantAsWeekDay,
	roundHoursToNearestQuarterDay,
	sortByInstantAscending,
} from './utilities';
import type { Temporal } from '@js-temporal/polyfill';
import type { Invoice, LineItem } from 'xero-node';
import type { ITimeEntry, ITimeEntryGroup, ITimeEntryGroupMap } from './timeEntries';

/**
 * Represents a time entry to go into a Xero invoice line item.
 */
export interface ILineItemTimeEntry {
	project: string;
	task: string;
	hours: number;
}

/**
 * Represents the metadata for a line item in an Xero invoice.
 */
export interface ILineItemMetadata {
	date: Temporal.Instant;
	totalHours: number;
	timeEntries: ILineItemTimeEntry[];
}

/**
 * Constants.
 */
const TENANT_ID = ``; // NOTE: This is always an empty string when using Xero custom integrations (client credentials grant type).
const LINE_ITEM_FIELD_SEP = `\n-----\n`;

/**
 * Xero client instant.
 */
const CLIENT = new XeroClient({
	clientId: config.env.XERO_APP_CLIENT_ID,
	clientSecret: config.env.XERO_APP_CLIENT_SECRET,
	grantType: `client_credentials`,
});

/**
 * Extracts a useful message from the given Xero error. Copes with Xero throwing objects instead of errors as well as
 * ordinary JS errors.
 * @param err - The error, string or object to extract the message from.
 */
// eslint-disable-next-line @typescript-eslint/no-explicit-any -- Xero SDK throws objects not errors.
function extractXeroErrorDetails(err: Error | string | any): string {
	if (err instanceof Error) return err.message;
	if (typeof err === `string`) return err;
	if (`response` in err && typeof err.response.body === `string`) return err.response.body;
	return `-unknown-`;
}

/**
 * Authenticates with the Xero API.
 */
export async function authenticate(): Promise<void> {
	try {
		log.info(`Authenticating with Xero`);
		await CLIENT.getClientCredentialsToken();
	} catch (e: unknown) {
		const details = extractXeroErrorDetails(e);
		throw new Error(`Failed to get Xero client credentials token => ${details}`);
	}
}

/**
 *  Returns an array of Xero invoices with draft status.
 */
async function getDraftInvoices(): Promise<Invoice[]> {
	try {
		log.info(`Getting all draft invoices from Xero`);

		const ifModifiedSince = undefined;
		const where = undefined;
		const order = `Date ASC`;
		const iDs = undefined;
		const invoiceNumbers = undefined;
		const contactIDs = undefined;
		const statuses = [`DRAFT`];
		const result = await CLIENT.accountingApi.getInvoices(
			TENANT_ID,
			ifModifiedSince,
			where,
			order,
			iDs,
			invoiceNumbers,
			contactIDs,
			statuses,
		);

		const invoices = result.body.invoices ?? [];

		log.info(`Found ${invoices.length} draft invoice(s)`);
		return invoices;
	} catch (e: unknown) {
		const details = extractXeroErrorDetails(e);
		throw new Error(`Failed to get draft invoices => ${details}`);
	}
}

/**
 * Gets the single draft invoice for the given contact that we want to modify.
 * @param contactId - The ID of the Xero contact to get invoices for.
 */
export async function getPendingInvoiceForContact(contactId: string): Promise<Invoice> {
	log.info(`Getting pending invoice from Xero for contact "${contactId}"`);

	const invoices = await getDraftInvoices();
	const clientInvoices = invoices.filter(invoice => invoice.contact?.contactID === contactId);
	const [invoice] = clientInvoices;
	if (clientInvoices.length > 1) throw new Error(`Multiple invoices for contact "${contactId}"`);
	if (!invoice) throw new Error(`No invoices for contact "${contactId}"`);

	log.info(`Found invoice "${invoice.invoiceNumber}" with ${invoice.lineItems?.length ?? 0} existing line item(s)`);
	return invoice;
}

/**
 * Maps a single time entry to a line item time entry.
 * @param timeEntry - The time entry to convert to a line item entry.
 */
function mapTimeEntryToXeroLineItemEntry(timeEntry: ITimeEntry): ILineItemTimeEntry {
	const { project, task, hours } = timeEntry;
	return { project, task, hours };
}

/**
 * Maps a single time entry group to a line item metadata object.
 * @param group - The group of time entries to convert to a line item.
 */
function mapGroupToXeroLineItemMetadata(group: ITimeEntryGroup): ILineItemMetadata {
	const timeEntries = group.timeEntries.map(mapTimeEntryToXeroLineItemEntry);
	return { date: group.date, totalHours: group.totalHours, timeEntries };
}

/**
 * Creates a line item description string from the given metadata object.
 * @param metadata - The metadata for the line item.
 */
function createLineItemDescription(metadata: ILineItemMetadata): string {
	const dateString = `${formatInstantAsDateString(metadata.date)} - ${formatInstantAsWeekDay(metadata.date)}`;
	const timeEntryStrings = metadata.timeEntries.map(entry =>
		[`Project: ${entry.project}`, `Task: ${entry.task}`, `Time: ${entry.hours} hour(s)`].join(`\n`),
	);
	const totalHoursString = `Total Time: ${metadata.totalHours} hour(s)`;
	return [dateString, ...timeEntryStrings, totalHoursString].join(LINE_ITEM_FIELD_SEP);
}

/**
 * Maps the given metadata object to a line item object.
 * @param metadata - The metadata for the line item.
 */
function mapMetadataToLineItem(metadata: ILineItemMetadata): LineItem {
	return {
		description: createLineItemDescription(metadata),
		quantity: roundHoursToNearestQuarterDay(metadata.totalHours),
		unitAmount: config.env.DAY_RATE,
		itemCode: config.env.XERO_LINE_ITEM_CODE,
		accountCode: config.env.XERO_LINE_ITEM_ACCOUNT_CODE,
		taxType: config.env.XERO_LINE_ITEM_TAX_TYPE,
	};
}

/**
 * Adds the given grouped time entries as line items to the given invoice, replacing any existing line items.
 * @param invoice - The invoice to add line items to.
 * @param groupMap - The map of time entry groups to add.
 */
export function addLineItemsToInvoice(invoice: Invoice, groupMap: ITimeEntryGroupMap): void {
	const newLineItems = Object.values(groupMap)
		.sort(sortByInstantAscending(`date`))
		.map(mapGroupToXeroLineItemMetadata)
		.map(mapMetadataToLineItem);

	log.info(`Generated ${newLineItems.length} line item(s) to add to invoice`);
	invoice.lineItems = newLineItems;
}

/**
 * Saves the given invoice, it must already exist in Xero.
 * @param invoice - The invoice we are updating.
 */
export async function updateInvoice(invoice: Invoice): Promise<void> {
	try {
		const invoiceId = invoice.invoiceID;
		if (!invoiceId) throw new Error(`Invoice is missing invoice ID`);

		log.info(`Updating invoice "${invoiceId}" with ${invoice.lineItems?.length ?? 0} line item(s)`);

		const data = { invoices: [invoice] };
		await CLIENT.accountingApi.updateInvoice(TENANT_ID, invoiceId, data);
	} catch (e: unknown) {
		const details = extractXeroErrorDetails(e);
		throw new Error(`Failed to update invoice => ${details}`);
	}
}
