import crypto from 'crypto';
import { formatInstantAsDateString } from './utilities';
import type { Temporal } from '@js-temporal/polyfill';

/**
 * Represents a single time entry.
 */
export interface ITimeEntry {
	id: string;
	date: Temporal.Instant;
	project: string;
	task: string;
	hours: number;
}

/**
 * Represents a group of time entries.
 */
export interface ITimeEntryGroup {
	date: Temporal.Instant;
	totalHours: number;
	timeEntries: ITimeEntry[];
}

/**
 * Represents a dictionary of groups keyed by date.
 */
export interface ITimeEntryGroupMap {
	[key: string]: ITimeEntryGroup;
}

/**
 * Generates a unique ID for a time entry based on the details given.
 * @param date - The date of the time entry.
 * @param project - The name of the project.
 * @param task - The description of the task completed.
 * @param hours - The number of hours logged.
 */
export function generateTimeEntryId(date: Temporal.Instant, project: string, task: string, hours: number): string {
	const dateStr = formatInstantAsDateString(date);
	const data = `${dateStr}-${project}-${task}-${hours}`;
	const hash = crypto.createHash(`shake256`, { outputLength: 8 }).update(data).digest(`hex`);

	return hash;
}

/**
 * Creates a new time entry object from the details given.
 * @param date - The date of the time entry.
 * @param project - The name of the project.
 * @param task - The description of the task completed.
 * @param hours - The number of hours logged.
 */
export function createTimeEntry(date: Temporal.Instant, project: string, task: string, hours: number): ITimeEntry {
	return {
		id: generateTimeEntryId(date, project, task, hours),
		date,
		project,
		task,
		hours,
	};
}

/**
 * Groups the given list of time entries by date.
 * @param timeEntries - The list of time entries to group.
 */
export function groupTimeEntriesByDate(timeEntries: ITimeEntry[]): ITimeEntryGroupMap {
	const groupMap: ITimeEntryGroupMap = timeEntries.reduce<ITimeEntryGroupMap>((acc, entry) => {
		const entryDateStr = formatInstantAsDateString(entry.date);
		const existingGroup = acc[entryDateStr];

		if (existingGroup) {
			existingGroup.totalHours += entry.hours;
			existingGroup.timeEntries.push(entry);
			return acc;
		}

		const newGroup = { date: entry.date, totalHours: entry.hours, timeEntries: [entry] };
		return { ...acc, [entryDateStr]: newGroup };
	}, {});

	return groupMap;
}
