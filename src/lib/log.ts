/* eslint-disable @typescript-eslint/no-explicit-any -- anything can be logged */

export function info(...args: any[]): void {
	process.stdout.write(`${args.join(` `)}\n`);
}

export function error(...args: any[]): void {
	process.stderr.write(`${args.join(` `)}\n`);
}

/* eslint-enable @typescript-eslint/no-explicit-any -- anything can be logged */
