import { Temporal } from '@js-temporal/polyfill';

/**
 * Represents a basic dictionary.
 */
export interface IDict {
	[key: string]: any; // eslint-disable-line @typescript-eslint/no-explicit-any -- can't predict what types the dictionary will contain.
}

/**
 * Generates a timestamp for the 1st of the given month and year.
 * @param year - The year to generate the timestamp for.
 * @param month - The month to generate the timestamp for.
 */
export function getTimestampForYearAndMonth(year: number, month: number): number {
	const dateStr = `${year}-${month.toString().padStart(2, `0`)}-01`;
	return new Date(dateStr).valueOf();
}

/**
 * Returns true if the given input string is a valid date string.
 * @param input - The string to check.
 */
export function isDateString(input: string): boolean {
	return /^\d{4}-\d{2}-\d{2}$/.test(input);
}

/**
 * Formats the given input instant as a date string.
 * @param input - The instant to convert to a date string.
 */
export function formatInstantAsDateString(input: Temporal.Instant): string {
	return input.toLocaleString(`en-GB`, { dateStyle: `short` });
}

/**
 * Formats the given input instant as a date string.
 * @param input - The instant to convert to a date string.
 */
export function formatInstantAsWeekDay(input: Temporal.Instant): string {
	return input.toLocaleString(`en-GB`, { weekday: `long` });
}

/**
 * Converts the given date string to an instant.
 * @param dateString - The date string to convert to an instant.
 */
export function instantFromDateString(dateString: string): Temporal.Instant {
	return Temporal.Instant.from(`${dateString}Z`);
}

/**
 * Returns true if the second instant is before the first instant.
 * @param instantA - The first instant to compare.
 * @param instantB - The second instant to compare.
 */
export function isInstantBefore(instantA: Temporal.Instant, instantB: Temporal.Instant): boolean {
	return Temporal.Instant.compare(instantA, instantB) === -1;
}

/**
 * Returns true if the second instant is after the first instant.
 * @param instantA - The first instant to compare.
 * @param instantB - The second instant to compare.
 */
export function isInstantAfter(instantA: Temporal.Instant, instantB: Temporal.Instant): boolean {
	return Temporal.Instant.compare(instantA, instantB) === 1;
}

/**
 * Boxes the given input list so that all items fall within the given time window.
 * @param input - The list of items to box.
 * @param key - The property of each item to use as the date.
 * @param windowStart - The start of the time window.
 * @param windowEnd - The end of the time window.
 */
export function timeBoxList<ListItem extends IDict>(
	input: ListItem[],
	key: string,
	windowStart: Temporal.Instant,
	windowEnd: Temporal.Instant,
): ListItem[] {
	const output: ListItem[] = [];

	for (const entry of input) {
		if (isInstantBefore(entry[key], windowStart)) continue;
		if (isInstantAfter(entry[key], windowEnd)) continue;
		output.push(entry);
	}

	return output;
}

/**
 * Factory for creating a new sort comparator that sorts in ascending order by date using the given property key.
 * @param key - The property of each item to use for sorting.
 */
export function sortByInstantAscending(key: string): (itemA: IDict, itemB: IDict) => number {
	return (itemA: IDict, itemB: IDict): number => {
		return Temporal.Instant.compare(itemA[key], itemB[key]);
	};
}

/**
 * Returns the number of days for the given number of hours, rounded up to the nearest quarter day.
 * @param hours - The value to round.
 */
export function roundHoursToNearestQuarterDay(hours: number): number {
	switch (true) {
		case hours <= 2:
			return 0.25;

		case hours <= 4:
			return 0.5;

		case hours <= 6:
			return 0.75;

		default:
			return 1;
	}
}
