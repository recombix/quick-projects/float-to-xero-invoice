import * as log from './log';

/**
 * Reads the year, month, and list of filenames from the args object.
 */
export function readCliArgs(): [number, number, string[]] {
	log.info(`Reading CLI args`);

	const [yearMonthStr, ...filenames] = process.argv.slice(2);
	log.info(yearMonthStr, filenames);

	const [, yearStr, monthStr] = yearMonthStr?.match(/^(\d+)-(\d+)$/i) ?? [];
	const year = Number.parseInt(yearStr ?? ``, 10);
	const month = Number.parseInt(monthStr ?? ``, 10);

	if (Number.isNaN(year) || Number.isNaN(month) || year < 2022 || month < 1 || month > 12) {
		throw new Error(`Year and month must be provided as the first argument in the format "2022-08"`);
	}

	if (!filenames.length) throw new Error(`Filenames must be provided`);

	log.info(`${filenames.length} filename(s) were provided`);
	return [year, month, filenames];
}
