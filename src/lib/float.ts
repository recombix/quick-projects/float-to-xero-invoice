import path from 'path';
import csv from 'csvtojson';
import { Temporal } from '@js-temporal/polyfill';
import { startOfMonth, endOfMonth } from 'date-fns';
import * as log from './log';
import { instantFromDateString, isDateString, timeBoxList, formatInstantAsDateString } from './utilities';
import { createTimeEntry } from './timeEntries';
import type { ITimeEntry } from './timeEntries';

/**
 * Represents a float time entry from the CSV file.
 */
interface IFloatTimeEntry {
	'Date': string;
	'Client': string;
	'Project': string;
	'Task': string;
	'Person': string;
	'Logged Billable hours': string; // eslint-disable-line @typescript-eslint/naming-convention -- must match CSV.
	'Logged NonBillable hours': string; // eslint-disable-line @typescript-eslint/naming-convention -- must match CSV.
	'Notes': string;
}

/**
 * Maps the given float time entry to an ordinary time entry.
 * @param input - The float time entry to map.
 */
function mapToTimeEntry(input: IFloatTimeEntry): ITimeEntry {
	const date = instantFromDateString(input.Date);
	const hours = Number.parseFloat(input[`Logged Billable hours`]);

	return createTimeEntry(date, input.Project, input.Task, hours);
}

/**
 * Filters out the total row and everything below it from the list of float time entries.
 * @param input - List of float time entries.
 */
function filterOutTotals(input: IFloatTimeEntry[]): IFloatTimeEntry[] {
	const output: IFloatTimeEntry[] = [];

	for (const row of input) {
		if (!isDateString(row.Date)) break;
		output.push(row);
	}

	return output;
}

/**
 * Reads in the given CSV file and parse it as time entries.
 * @param filename - The name of the CSV file to read.
 */
async function readCsvFile(filename: string): Promise<ITimeEntry[]> {
	log.info(`Reading CSV file "${filename}"`);

	const filePath = path.resolve(__dirname, `..`, `..`, `data`, filename);
	const allRows = await csv().fromFile(filePath);
	const rows = filterOutTotals(allRows);
	const timeEntries = rows.map(mapToTimeEntry);

	return timeEntries;
}

/**
 * Reads in multiple CSV files and parses them as time entries, returning a timeboxed window of time entries based on
 * the month indicated by the given timestamp.
 * @param filenames - List of filenames of CSV files to read.
 * @param runTimestamp - The timestamp to indicate the month for the time window.
 */
export async function readCsvFilesForMonth(filenames: string[], runTimestamp: number): Promise<ITimeEntry[]> {
	log.info(`Reading ${filenames.length} CSV files`);

	const windowStart = Temporal.Instant.from(startOfMonth(runTimestamp).toISOString());
	const windowEnd = Temporal.Instant.from(endOfMonth(runTimestamp).toISOString());
	log.info(`Time box is ${formatInstantAsDateString(windowStart)} -> ${formatInstantAsDateString(windowEnd)}`);

	const promises = filenames.map(async filename => readCsvFile(filename));
	const timeEntries = (await Promise.all(promises)).flat();
	const timeBoxedEntries = timeBoxList(timeEntries, `date`, windowStart, windowEnd);

	log.info(`Found ${timeEntries.length} total time entries`);
	log.info(`Found ${timeBoxedEntries.length} time entries in the window`);
	return timeBoxedEntries;
}
