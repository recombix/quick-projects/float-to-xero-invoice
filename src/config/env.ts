import { EnvironmentLoader } from 'safe-env-vars';

const env = new EnvironmentLoader();

export const XERO_APP_CLIENT_ID = env.string.get(`XERO_APP_CLIENT_ID`);
export const XERO_APP_CLIENT_SECRET = env.string.get(`XERO_APP_CLIENT_SECRET`);
export const XERO_CONTACT_ID = env.string.get(`XERO_CONTACT_ID`);
export const XERO_LINE_ITEM_CODE = env.string.get(`XERO_LINE_ITEM_CODE`);
export const XERO_LINE_ITEM_TAX_TYPE = env.string.get(`XERO_LINE_ITEM_TAX_TYPE`);
export const XERO_LINE_ITEM_ACCOUNT_CODE = env.string.get(`XERO_LINE_ITEM_ACCOUNT_CODE`);
export const DAY_RATE = env.number.get(`DAY_RATE`);
